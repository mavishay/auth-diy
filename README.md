# Auth DIY is a lecture made by Avishay Maor at Tikal
![Auth DIY is a lecture made by Avishay Maor at Tikal](./2022-06-01_11-02-18.png)

This lecture includes:
* A presentation about the theoretical part the Authentication & Authorization
* A hands-on workshop where we build auth system with NextJS

### The workshop divided to 6 steps
1. [Create the auth pages in the ui](https://gitlab.com/mavishay/auth-diy/-/blob/main/docs/1.auth-pages.md)
2. [Create client auth guard that check if user is authenticated](https://gitlab.com/mavishay/auth-diy/-/blob/main/docs/2.client-auth-gaurd.md)
3. [Lift Postgres DB with user model using Prisma](https://gitlab.com/mavishay/auth-diy/-/blob/main/docs/3.database.md)
4. [Create auth API routes](https://gitlab.com/mavishay/auth-diy/-/blob/main/docs/4.api-endpoints.md)
5. [Create and use a cookie based token](https://gitlab.com/mavishay/auth-diy/-/blob/main/docs/5.cookie-based-token.md)
6. [Create an auth guard API middleware](https://gitlab.com/mavishay/auth-diy/-/blob/main/docs/6.api-auth-guard.md)

### What you will find in this Repo
* A typescript `create-next-app` app.
* `package.json` file with all the dependencies we will use in the workshop.
* a `utils` directory that contains utilities that we will use during the workshop.
* a `docs` directory that contains `md` files that we will use during the workshop.


If you get stuck during the workshop, you can checkout the branch `final result` to see a working sample of our workshop.

---

# Happy codding...........